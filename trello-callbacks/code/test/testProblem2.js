const getListsInfo = require("../problem2.js") // importing getListsInfo

const lists = require("../../data/lists.json") //importing lists data

function getResult(getListsInfo, lists, id){
    setTimeout(()=>{
        const result = getListsInfo(lists,id)
        console.log(result)
    },2000)
}

getResult(getListsInfo,lists, "mcu453ed") //calling getResult by sending "getListsInfo" as a parameter.