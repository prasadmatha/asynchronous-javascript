const getBoardsInfo = require("../problem1.js") // importing getBoardsInfo

const boards = require("../../data/boards.json") // importing boards data

function getResult(getBoardsInfo, boards, id){
    setTimeout(()=>{
        const result = getBoardsInfo(boards,id)
        console.log(result)
    },2000)
}

getResult(getBoardsInfo,boards, "mcu453ed") //calling getResult by sending "getBoardsInfo" as a parameter.