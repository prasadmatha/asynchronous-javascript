const getCardsInfo = require("../problem3.js") // importing getCardsInfo

const cards = require("../../data/cards.json") //importing cards data

function getResult(getCardsInfo, cards, id){
    setTimeout(()=>{
        const result = getCardsInfo(cards,id)
        console.log(result)
    },2000)
}

getResult(getCardsInfo,cards, "qwsa221") //calling getResult by sending "getCardsInfo" as a parameter.

