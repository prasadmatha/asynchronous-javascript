
/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/



const boards = require("../data/boards.json") // imporing boards data

const lists = require("../data/lists.json") // imporing lists data

const cards = require("../data/cards.json") //importing cards data

function getInfo(getBoardsInfo,getListsInfo,getCardsInfo,name){ //declaring getInfo to make use of functions in problem 1, 2 & 3.
    
    let boardData = boards.filter(each => each.name === name) //getting board data based on name.
    
    let id = boardData[0].id // getting board id and assigning to variable "id"
   
    setTimeout(()=>{
        const result = getBoardsInfo(boards,id) // getting boards info
        console.log(result)
        console.log("\n")
    },2000)
    
    setTimeout(()=>{
        const result = getListsInfo(lists,id) // getting lists info
        console.log(result)
        console.log("\n")
    },4000)
    
    let arrayLists = Object.entries(lists) //converting lists into arrays
    
    const listsInfo = arrayLists.map(each => [...each[1]]).flat() // getting elements in lists
    
    const cardId = listsInfo.reduce((acc,curr)=>{ // initializing cardId with cardId in lists
        if (curr.name === "Mind" || curr.name === "Space"){
            acc.push(curr.id)
        }
        return acc
    },[])
    
    setTimeout(()=>{
        const result = getCardsInfo(cards,cardId) //getting cards info
        console.log(result)
        console.log("\n")
    },6000)

}

module.exports = getInfo    //exporting getInfo