/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the 
    boardID that is passed to it from the given data in lists.json. Then pass control back to the code 
    that called it by using a callback function.
*/

function getListsInfo(lists,id) { //getting lists info by lists id

    const result = lists[id] //getting lists info based on board id
    return result //returning result
    
}

module.exports = getListsInfo   //exporting getListsInfo