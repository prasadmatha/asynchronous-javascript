const fs = require("fs"); //importing "fs" module.

const randomJSON = require("../problem1.js") //importing "randomJSON" from "problem1.js"

function createFolder() {   //function to create folder
    fs.mkdir("../../randomJSONFiles", (err => {
        if (err) {
            console.error(err) // error
            return
        }
        console.log(`folder "randomJSONFiles" is created successfully`)
    }
    ))
}



function createFiles(n,deletefiles) {   //function to create files upto n.
    for (let i = 1; i < n + 1; i++) {
        fs.appendFile(`../../randomJSONFiles/sample${i}.json`, 'Hello content!', function (err) {
            filesCreated = true
            if (err) {
                console.error(err); //error
                return
            }
            console.log(`file "sample${i}.json" is created successfully`)
            if (i === n){
                deleteFiles(n)
            }
        });

    }
}


function deleteFiles(n) {   //function to delete files
    for (let i = 1; i < n + 1; i++) {
        fs.unlink(`../../randomJSONFiles/sample${i}.json`, (err => {
            if (err) {
                console.error(err)
                return
            }
            console.log(`file "sample${i}.json" is deleted successfully`)
        }))
    }
}


randomJSON(createFolder)    //calling randomJSON with the parameter "createFolder"
randomJSON(createFiles, 4)  //calling randomJSON with the parameter "createFiles"
