/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs")    //importing "fs" module.

function fileSystem(filePath) {

    //1. Reading data of lipsum.txt
    fs.readFile(filePath, "utf-8", function (err, data) { // read "lipsum.txt"
        if (err) {

            console.error(err)
            return

        }

        //2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        let convertedText = data.toUpperCase()
        fs.writeFile("sample1.txt", convertedText, function (err) {

            if (err) {

                console.error(err)
                return

            }
            fs.writeFile("fileNames.txt", "sample1.txt", function (err) {

                if (err) {

                    console.error(err)
                    return

                }

            })
            //3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
            fs.readFile("sample1.txt", "utf-8", function (err, data) {

                if (err) {

                    console.error(err)
                    return

                }

                data = data.toLowerCase()
                let lowercase = data.split(" ")
                lowercase = JSON.stringify(lowercase)
                fs.writeFile("sample2.txt", lowercase, function (err) {

                    if (err) {

                        console.error(err)
                        return

                    }
                    fs.appendFile("fileNames.txt", `\nsample2.txt`, function (err) {

                        if (err) {

                            console.error(err)
                            return

                        }
                        //4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
                        fs.readFile("sample2.txt", "utf-8", function (err, data) {

                            if (err) {

                                console.error(err)
                                return

                            }
                            data = JSON.parse(data)
                            data = data.sort()
                            data = JSON.stringify(data)
                            fs.writeFile("sample3.txt", data, function (err) {

                                if (err) {

                                    console.error(err)
                                    return

                                }
                                fs.appendFile("fileNames.txt", `\nsample3.txt`, function (err) {

                                    if (err) {

                                        console.error(err)
                                        return

                                    }

                                    //5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
                                    fs.readFile("fileNames.txt","utf-8",function(arr,data){

                                        if(err){

                                            console.error(err)
                                            return

                                        }
                                        let arrayOfFilesNames = data.split("\n")
                                        for (let i = 0 ; i < arrayOfFilesNames.length ; i++){

                                            fs.unlink(`${arrayOfFilesNames[i]}`,function(err){

                                                if (err){

                                                    console.error(err)
                                                    return

                                                }

                                            })

                                        }
                                        
                                    })
                                })
                            })
                        })
                    })
                })
            })
        })
    })
}



module.exports = fileSystem